<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>

<body>
    <h2>Buat Account Baru!</h2>
    <h4>Sign Up Form</h4>
    <form method="POST" action="{{url('welcome')}}">
        {{csrf_field()}}
        <p>
            <label for="firstName">First name:</label><br>
            <input type="text" name="firstName" id="firstName">
        </p>

        <p>
            <label for="lastName">Last name:</label><br>
            <input type="text" name="lastName" id="lastName">
        </p>

        <p>
            Gender: <br>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label>
        </p>

        <p>
            Nationality: <br>
            <select name="nationality" id="nationality">
                <option value="id">Indonesian</option>
                <option value="sg">Singaporean</option>
                <option value="my">Malaysian</option>
                <option value="au">Australian</option>
            </select>
        </p>

        <p>
            Bio: <br>
            <textarea name="bio" id="" cols="30" rows="10"></textarea>
        </p>

        <input type="submit" value="Sign Up">
    </form>
</body>

</html>