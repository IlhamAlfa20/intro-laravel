<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register()
    {
        return view('register');
    }

    public function Welcome(Request $request)
    {
        $firstName = $request -> input('firstName');
        $lastName = $request->input('lastName');
        return view('welcome', compact('firstName', 'lastName'));
    }
}
